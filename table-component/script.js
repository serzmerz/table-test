(function() {

    function addColumn() {
        const trItems = document.querySelectorAll('.b-container__b-tr');

        for (let item of trItems) {
            let element = document.createElement('td');
            element.className = 'b-container__item';
            item.appendChild(element);
        }
    }

    function addRow() {

        const tdItemsCount = document.querySelector('.b-container__b-tr').childElementCount;
        const table = document.querySelector('.b-container__b-table').firstElementChild;

        const newTr = document.createElement('tr');
        newTr.className = 'b-container__b-tr';

        for (let i = 0; i < tdItemsCount; i++) {
            let element = document.createElement('td');
            element.className = 'b-container__item';
            newTr.appendChild(element);
        }

        table.appendChild(newTr);
    }

    function showRemoveBtn() {
        const table = document.querySelector('.b-container__b-table');

        if (table.querySelector('.b-container__b-tr').childElementCount > 1) {
            const btnRemoveColumn = document.querySelector('.b-container__b-remove-column');
            btnRemoveColumn.classList.remove("b-container__b-btn-hidden");
        }
        if (table.rows.length > 1) {
            const btnRemoveRow = document.querySelector('.b-container__b-remove-row');
            btnRemoveRow.classList.remove("b-container__b-btn-hidden");
        }
    }

    function moveRemoveBtn() {

        showRemoveBtn();
        const target = event.target;
        if (target.tagName === 'TD') {
            btnRemoveColumn.style.marginLeft = 54.5 * target.cellIndex + 54.5 + "px";
            //btnRemoveRow.style.marginTop = 54.5*target.parentNode.rowIndex+"px";
            btnRemoveRow.style.transform = `translateY(${54.5 * target.parentNode.rowIndex}px)`;
        }

    }

    function hideRemoveBtn() {
        const btnRemoveColumn = document.querySelector('.b-container__b-remove-column');
        btnRemoveColumn.classList.add("b-container__b-btn-hidden");
        const btnRemoveRow = document.querySelector('.b-container__b-remove-row');
        btnRemoveRow.classList.add("b-container__b-btn-hidden");
    }

    function removeRow() {
        document.querySelector('.b-container__b-table').firstElementChild.lastElementChild.remove();
        const btnRemoveRow = document.querySelector('.b-container__b-remove-row');
        btnRemoveRow.classList.add("b-container__b-btn-hidden");
    }

    function removeColumn() {
        const trItems = document.querySelectorAll('.b-container__b-tr');
        for (let item of trItems) {
            item.lastElementChild.remove();
        }
        const btnRemoveColumn = document.querySelector('.b-container__b-remove-column');
        btnRemoveColumn.classList.add("b-container__b-btn-hidden");
    }


    const app = document.querySelector('app-root');
    app.innerHTML = '<div class="b-container">' +
        '<button class="b-container__b-btn b-container__b-remove b-container__b-remove-column b-container__b-btn-hidden">-</button>' +
        '<div class="b-container__b-wrap-table">' +
        '<button class="b-container__b-btn b-container__b-remove b-container__b-remove-row b-container__b-btn-hidden">-</button>' +
        '<table class="b-container__b-table">' +
        '<tr class="b-container__b-tr">' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '</tr>' +
        '<tr class="b-container__b-tr">' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '</tr>' +
        '<tr class="b-container__b-tr">' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '<td class="b-container__item"></td>' +
        '</tr>' +
        '</table>' +
        '<button class="b-container__b-btn b-container__b-column">+</button>' +
        '</div>' +
        '<button class="b-container__b-btn b-container__b-row">+</button>' +
        '</div>';

    const btnAddColumn = document.querySelector('.b-container__b-column');
    const btnAddRow = document.querySelector('.b-container__b-row');
    const btnRemoveColumn = document.querySelector('.b-container__b-remove-column');
    const btnRemoveRow = document.querySelector('.b-container__b-remove-row');
    const table = document.querySelector('.b-container__b-table');

    btnAddColumn.addEventListener('click', addColumn);
    btnAddRow.addEventListener('click', addRow);
    table.addEventListener('mouseover', moveRemoveBtn);
    table.addEventListener('mouseout', hideRemoveBtn);

    btnRemoveRow.addEventListener('mouseover', showRemoveBtn);
    btnRemoveRow.addEventListener('mouseout', hideRemoveBtn);
    btnRemoveRow.addEventListener('click', removeRow);

    btnRemoveColumn.addEventListener('mouseover', showRemoveBtn);
    btnRemoveColumn.addEventListener('mouseout', hideRemoveBtn);
    btnRemoveColumn.addEventListener('click', removeColumn);

})();

